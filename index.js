const DAYS = ['donnerstag', 'freitag', 'samstag', 'sonntag'];

const getElementId = (identifier, day, from) => `${day}-${identifier}-${from}`;

const addDemandToNames = (names, demand) => `${names} (${demand})`;

function splitByKey(keys, array) {
  const result = {};
  const splitIndexes = [];

  for (let index = 0; index < array.length; index++) {
    if (keys.includes(array[index])) {
      splitIndexes.push(index);
    }
  }

  while (splitIndexes.length) {
    const currentIndex = splitIndexes.shift();

    result[array[currentIndex]] = array.slice(currentIndex, splitIndexes[0]);
  }

  return result;
}

function renderShiftCell(cell, volunteers, demand = Number.MAX_SAFE_INTEGER) {
  const volunteerCount = volunteers.split(',').filter(str => str.length).length;

  let demandIndicatorClass = '';
  let text = addDemandToNames(volunteers, demand - volunteerCount);

  if (volunteerCount === 0) {
    demandIndicatorClass = 'no-volunteers';
  } else if (volunteerCount < demand) {
    demandIndicatorClass = 'missing-volunteers';
  } else {
    demandIndicatorClass = 'enough-volunteers';
    text = volunteers;
  }

  cell.addClass(demandIndicatorClass);
  cell.text(text);
}

function processAndRender({ columns, elements }) {
  const [timeColumn, ...rawDays] = columns;
  const [jobs, demands, ...plans] = elements;

  const dayColumnNames = splitByKey(DAYS, rawDays);

  plans.forEach(timeSlot => {
    const from = timeSlot[timeColumn];

    DAYS.forEach(day => {
      const possibleElementIds = dayColumnNames[day].reduce(
        (result, currentColumn) => {
          const jobName = jobs[currentColumn].toLowerCase();

          result.push({
            column: currentColumn,
            id: getElementId(jobName, day, from),
          });

          return result;
        },
        []
      );

      possibleElementIds.forEach(({ column, id }) => {
        const possibleElement = $(`#${id}`);
        if (possibleElement.length) {
          renderShiftCell(possibleElement, timeSlot[column], demands[column]);
        }
      });
    });
  });
}

function renderLoadingStatus(isLoading) {
  if (isLoading) {
    $('.loading-shifts').before();
  } else {
    $('.loading-shifts').hide();
  }
}

function fetchData(dataUrl) {
  const elements = [];

  const postProcess = (onSuccess, onError) => data => {
    if (!data) onError('No data returned.');

    onSuccess(
      Object.keys(data).map(shift => ({
        shift,
        columns: data[shift].column_names,
        elements: data[shift].elements,
      }))
    );
  };

  return new Promise((resolve, reject) => {
    Tabletop.init({
      key: dataUrl,

      callback: postProcess(resolve, reject),
      simpleSheet: false,
      prettyColumnNames: false,
      parseNumbers: true,
      postProcess: function(element) {
        elements.push(element);
      },
    });
  });
}

$(document).ready(() => {
  renderLoadingStatus(true);

  const spreadSheetUrl = $('[data-spreadsheet-url]')
    .first()
    .data('spreadsheet-url');

  fetchData(spreadSheetUrl).then(shifts => {
    shifts.forEach(processAndRender);
    renderLoadingStatus(false);
  }, console.warn);
});
